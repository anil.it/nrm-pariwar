<?php

namespace NRMPariwar\MagazineBundle\TwigExtension;


use Twig_Extension;
use Twig_SimpleFunction;
use NRMPariwar\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ToggleExtension extends Twig_Extension
{

    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('status_info', [$this, 'statusInfo']),
        ];
    }


    public function statusInfo($status, $callAjax = false, $entity = '', $id = '', $property = '')
    {

        $class = ($status) ? 'label-success' : 'label-warning';
        $label = ($status) ? 'YES' : 'NO';
        $out = '<span class="label ' . $class . '">' . $label . '</span>';

        if ($callAjax) {
            $html = '<a href="#" data-toggle="myAjax" data-entity="' . $entity . '" data-id="' . $id . '"';
            $html .= ' data-status = "' . $status . '" data-property="' . $property . '">';
            $html .= $out;
            $html .= '</a>';
        } else {
            $html = $out;
        }


        return $html;
    }

    public function getName()
    {
        return 'nrm_toggle';
    }
}
//onclick="return confirm('Are you sure? Do you want to delete this news permanently?');">

