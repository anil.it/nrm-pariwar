<?php

namespace NRMPariwar\MagazineBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Attachment
 *
 * @ORM\Table(name="nrm_magazine_attachment")
 * @ORM\Entity(repositoryClass="NRMPariwar\MagazineBundle\Repository\AttachmentRepository")
 */
class Attachment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=255)
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="zoom_url", type="string", length=255)
     */
    private $zoomUrl;

    /**
     * @var int
     *
     * @ORM\Column(name="ordering", type="integer")
     */
    private $ordering;

    /**
     * @return mixed
     */
    public function getMagazine()
    {
        return $this->magazine;
    }

    /**
     * @param mixed $magazine
     */
    public function setMagazine($magazine)
    {
        $this->magazine = $magazine;
    }

    /**
     * @param mixed $magazine
     */
    public function removeMagazine($magazine)
    {
        $this->remo = $magazine;
    }


    /**
     * One Magazine can have Many Attachments
     * @ORM\ManyToOne(targetEntity="NRMPariwar\MagazineBundle\Entity\Magazine", inversedBy="attachments")
     */
    private $magazine;




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return Attachment
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set zoomUrl
     *
     * @param string $zoomUrl
     *
     * @return Attachment
     */
    public function setZoomUrl($zoomUrl)
    {
        $this->zoomUrl = $zoomUrl;

        return $this;
    }

    /**
     * Get zoomUrl
     *
     * @return string
     */
    public function getZoomUrl()
    {
        return $this->zoomUrl;
    }


    /**
     * Set ordering
     *
     * @param integer $ordering
     *
     * @return Attachment
     */
    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;

        return $this;
    }

    /**
     * Get ordering
     *
     * @return int
     */
    public function getOrdering()
    {
        return $this->ordering;
    }
}

