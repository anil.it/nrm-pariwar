<?php

namespace NRMPariwar\MagazineBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NRMPariwar\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Magazine
 *
 * @ORM\Table(name="nrm_magazine")
 * @ORM\Entity(repositoryClass="NRMPariwar\MagazineBundle\Repository\MagazineRepository")
 */
class Magazine
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime",nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime",nullable=true)
     */
    private $updated;

    /**
     * @var int
     *
     * @ORM\Column(name="book_width", type="integer")
     */
    private $bookWidth;

    /**
     * @var int
     *
     * @ORM\Column(name="book_height", type="integer")
     */
    private $bookHeight;

    /**
     * @var string
     *
     * @ORM\Column(name="preview_image", type="string", length=255)
     * @Assert\NotBlank(message="Please, upload the previewImage.")
     * @Assert\File(mimeTypes={ "image/jpeg", "image/jpg", "image/png", "image/gif" })
     */
    private $previewImage;

    /**
     * @var int
     *
     * @ORM\Column(name="ordering", type="integer",nullable=true)
     */
    private $ordering;

    /**
     * @var bool
     *
     * @ORM\Column(name="published", type="boolean")
     */
    private $published=false;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted=false;

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=255)
     */
    private $alias;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="NRMPariwar\UserBundle\Entity\User")
     */
    private $createdBy;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="NRMPariwar\MagazineBundle\Entity\Category")
     */
    private $category;

    /**
     * One Magazine can have Many Attachments
     * @ORM\OneToMany(targetEntity="NRMPariwar\MagazineBundle\Entity\Attachment", mappedBy="magazine",cascade={"persist", "remove"})
     */
    private $attachments;

    public function __construct()
    {
        $this->attachments = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }
    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Magazine
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set bookWidth
     *
     * @param integer $bookWidth
     *
     * @return Magazine
     */
    public function setBookWidth($bookWidth)
    {
        $this->bookWidth = $bookWidth;

        return $this;
    }

    /**
     * Get bookWidth
     *
     * @return int
     */
    public function getBookWidth()
    {
        return $this->bookWidth;
    }

    /**
     * Set bookHeight
     *
     * @param integer $bookHeight
     *
     * @return Magazine
     */
    public function setBookHeight($bookHeight)
    {
        $this->bookHeight = $bookHeight;

        return $this;
    }

    /**
     * Get bookHeight
     *
     * @return int
     */
    public function getBookHeight()
    {
        return $this->bookHeight;
    }

    /**
     * Set previewImage
     *
     * @param string $previewImage
     *
     * @return Magazine
     */
    public function setPreviewImage($previewImage)
    {
        $this->previewImage = $previewImage;

        return $this;
    }

    /**
     * Get previewImage
     *
     * @return string
     */
    public function getPreviewImage()
    {
        return $this->previewImage;
    }

    /**
     * Set ordering
     *
     * @param integer $ordering
     *
     * @return Magazine
     */
    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;

        return $this;
    }

    /**
     * Get ordering
     *
     * @return int
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

    /**
     * Set published
     *
     * @param boolean $published
     *
     * @return Magazine
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return bool
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Magazine
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return Magazine
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return mixed
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    public function addAttachment($attachments)
    {
        if (!$this->hasAttachment($attachments)) {
            $attachments->setMagazine($this);
            $this->attachments[] = $attachments;
        }
        return $this;
    }

    public function hasAttachment(Attachment $attachments)
    {
        return $this->attachments->contains($attachments);
    }

    public function removeAttachment($attachments)
    {
        if ($this->hasAttachment($attachments)) {
            $this->attachments->removeElement($attachments);
        }

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

}

