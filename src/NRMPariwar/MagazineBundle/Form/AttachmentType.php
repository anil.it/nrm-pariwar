<?php
namespace NRMPariwar\MagazineBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class AttachmentType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('zoomUrl', TextType::class, [
                'label' => 'Zoom URL',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('file', TextType::class, [
                'label' => 'Image URL',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('ordering',IntegerType::class, [
                'label' => 'Ordering',
                'required' => false,
                'attr' => [
                    'class' => 'form-control '
                ]
            ])

        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => \NRMPariwar\MagazineBundle\Entity\Attachment::class

        ]);
    }



}




