<?php
namespace NRMPariwar\MagazineBundle\Form;

use Doctrine\DBAL\Types\StringType;
use FM\ElfinderBundle\Form\Type\ElFinderType;
use NRMPariwar\MagazineBundle\Entity\Attachment;
use NRMPariwar\MagazineBundle\Entity\Category;
use NRMPariwar\MagazineBundle\Entity\Magazine;
use NRMPariwar\MagazineBundle\Repository\CategoryRepository;
use NRMPariwar\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class MagazineType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('title', TextType::class, [
                'label' => 'Title',
                'required' => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('alias', TextType::class, [
                'label' => 'Alias',
                'required' => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('previewImage', FileType::class, [
                'data_class' => null,
                'property_path' => 'previewImage',
                'label' => 'Profile Pic',
//                'data'=>null,
                'required' => true,
//                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Click here to browse image.'
                ]
            ])
//            ->add('profileUrl', ElFinderType::class, [
//                'label' => 'Profile Pic',
//                'required' => false,
//                'instance' => 'form',
//                'enable' => true,
//                'attr' => [
//                    'class' => 'form-control featuredImage',
//                    'readonly' => 'readonly',
//                    'placeholder' => 'Click here to browse image.'
//                ]
//            ])
//
//            ->add('previewImage', ElFinderType::class, [
//                'label' => 'Profile Pic',
//                'required' => false,
//                'instance' => 'form',
//                'enable' => true,
//                'attr' => [
//                    'class' => 'form-control featuredImage',
//                    'placeholder' => 'Click here to browse image.'
//                ]
//            ])

            ->add('attachments', CollectionType::class, [
                'label' => 'Add Days',
                'entry_type'   => AttachmentType::class,
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'prototype'=>true,
            ])
            ->add('createdBy','entity', [
                'label' => 'Created By',
                'required' => false,
                'class' => User::class,
                'attr' => [
                    'class' => 'form-control '
                ]
            ])
            ->add('category','entity', [
                'label' => 'Category',
                'required' => false,
                'class' => Category::class,
                'query_builder' => function (CategoryRepository $repository) {
                    return $repository->createQueryBuilder('c')
                        ->where('c.deleted=false');
                },
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('created',DateTimeType::class, [
                'label' => 'Created Date',
                'widget'=>'single_text',
                'format'=>'y-M-d',
                'attr' => [
                    'class' => 'form-control createdDate',
                    'readonly' => 'readonly',
                    'data-date-format'=>'yyyy-mm-dd'
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Save',
                'attr' => [
                    'class' => 'form-control btn btn-primary'
                ]
            ])



        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Magazine::class
        ]);
    }



}




