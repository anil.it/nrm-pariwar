<?php

namespace NRMPariwar\MagazineBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploaderService
{
    private $targetDir;

    private $targetName;

    public function setTargetDir($targetDir,$targetName)
    {
        $this->targetDir = $targetDir;
        $this->targetName = $targetName;
    }


    public function upload(UploadedFile $file,$resize)
    {
        $fileName = $this->targetName.'.'.$file->guessExtension();

        $file->move($this->targetDir, $fileName);

        $resize->resizePreview($this->targetDir.$fileName);

        return $fileName;
    }

    public function uploadFile(UploadedFile $file,$resize)
    {
        $fileName = 'zoom_'.$this->targetName.'.'.$file->guessExtension();
        $FileName = $this->targetName.'.'.$file->guessExtension();

//        $resize->resize($this->targetDir.$fileName, $this->targetDir.$fileName.$FileName);

        $file->move($this->targetDir, $fileName);
        $resize->resize($this->targetDir.$fileName, $this->targetDir.$FileName);


        return [$fileName,$FileName];
    }

    public function getTargetDir()
    {
        return $this->targetDir;
    }
}