<?php

namespace NRMPariwar\MagazineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AjaxController extends Controller
{

    public function addImageAction(Request $request)
    {

        $files = $request->files;

        $dated =$request->get('someParameter')['dated'];

        if($dated){
            $thumbDir = $this->get('kernel')->getRootDir() . '/../web/uploads/media/' .$dated.'/nagarik/';
            $saveDir = $dated.'/nagarik/';


        }else{
            $dated = new \DateTime('now');
            $thumbDir = $this->get('kernel')->getRootDir() . '/../web/uploads/media/' .$dated->format('Y_M_d').'/nagarik/';
            $saveDir = $dated->format('Y_M_d').'/nagarik/';
        }

        $count = 0;
        $list =[];
        foreach($files as $file) {
            foreach ($file as $uploadedFile) {
                foreach ($uploadedFile as $filB) {
                $count += 1;
                $uploader = $this->get('nrm_pariwar.brochures_uploader');
                $uploader->setTargetDir($thumbDir, $count);

                $resize= $this->get('resizer');

//                $thumb = $uploader->uploadFile($uploadedFile,$resize);

                $thumb = $uploader->uploadFile($filB,$resize);
                $fileA['large'] =$saveDir . $thumb[0];
                $fileA['small'] =$saveDir . $thumb[1];
                $list[]=$fileA;
                }


            }
        }
        $response['list'] =$list;
        $response['status']= "success";
        $response['files']= $this->renderView('NRMPariwarMagazineBundle:Magazine:addImage.html.twig',['images'=>$list]);
        return new JsonResponse($response);
    }

    public function toggleAction(Request $request)
    {
        $entityClass = $request->get('entity');
        $property = $request->get('property');
        $entityId = $request->get('id');
        $currentStatus = (boolean)$request->get('status');

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($entityClass)->find($entityId);

        if (!$entity) {
            return new JsonResponse(['status' => $currentStatus], 400);
        }

        $getMethod = 'get' . ucfirst($property);
        $setMethod = 'set' . ucfirst($property);

        $entity->$setMethod(!$entity->$getMethod());

        $em->persist($entity);
        try {
            $em->flush();

            return new JsonResponse(['status' => $entity->$getMethod()]);
        } catch (\Exception $e) {
            return new JsonResponse(['status' => $entity->$getMethod()], 400);
        }
    }

}
