<?php

namespace NRMPariwar\MagazineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('NRMPariwarMagazineBundle:Default:index.html.twig');
    }
}
