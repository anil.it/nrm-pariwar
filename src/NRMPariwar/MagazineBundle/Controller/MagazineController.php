<?php

namespace NRMPariwar\MagazineBundle\Controller;

use NRMPariwar\MagazineBundle\Entity\Attachment;
use NRMPariwar\MagazineBundle\Entity\Category;
use NRMPariwar\MagazineBundle\Entity\Magazine;
use NRMPariwar\MagazineBundle\Form\CategoryType;
use NRMPariwar\MagazineBundle\Form\MagazineType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MagazineController extends Controller
{


    public  function listAction(Request $request)
    {

        $qb=$this->get('em')->getRepo(Magazine::class)->getAll();
        $paginator  = $this->get('knp_paginator');
        $data['magazines'] = $paginator->paginate($qb,$request->query->getInt('page', 1), 3);

        $data['place']='Magazine ';
        $data['title']='List';
        return $this->render('NRMPariwarMagazineBundle:Magazine:list.html.twig',$data);

    }

    public function setTarget($file)
    {
        $date = new \DateTime('now');
        $thumbDir = $this->get('kernel')->getRootDir() . '/../web/uploads/media/' .$date->format('Y_M_d').'/thumb/';
        $saveDir = $date->format('Y_M_d').'/thumb/';
        $uploader =  $this->get('nrm_pariwar.brochures_uploader');
        $uploader->setTargetDir($thumbDir,'nagarik');

        $resize= $this->get('resizer');
        $thumb = $uploader->upload($file,$resize);
        return ($saveDir.$thumb);

    }

    public function addAction(Request $request)
    {
        $id = $request->get('id');
        $images=[];

        if($id) {
            $magazine = $this->get('em')->find(Magazine::class, $id);
            $filePath = $this->container->getParameter("kernel.root_dir") . "/../web/uploads/media/";
            $url = "http://localhost:7080/uploads/media/";
            foreach ($magazine->getAttachments() as $image) {
                $sizes ='';
                if(file_exists($filePath.$image->getZoomUrl())){ $sizes= filesize($filePath.$image->getZoomUrl()); }
                $images[] = [
                    "id" => $image->getId(),
                    "name" => $image->getFile(),
                    "size" => $sizes,
                    "thumbnail" => $url.$image->getZoomUrl()
                ];
            }

            if ($magazine->isDeleted() == true) {
                $this->addFlash('error', 'Not Existed!!');
                return $this->redirectToRoute('nrm_pariwar_magazine_list');
            }
        }else{

            $magazine = new Magazine();
        }

        $form = $this->createForm(MagazineType::class,$magazine);
        $form->handleRequest($request);
//        if($form->isSubmitted()&& $form->isValid()) {

        if($form->isSubmitted()&& $request->getMethod()=="POST") {
            $magazine = $form->getData();
            $thumb = $this->setTarget($magazine->getPreviewImage());
            $magazine->setPreviewImage($thumb);
//            dump($magazine);
//            die();

            if(!$id){
//                $thumb = $this->setTarget($magazine->getPreviewImage());
//                $magazine->setPreviewImage($thumb);
                $magazine->setCreated(new \DateTime('now'));
            }
            else{

                $images = $request->get('removeImage');
                if(is_array($images)){
                    foreach($images as $image) {
                        $this->removeImage($image,$magazine);
                    }
                }
                else{
                    $this->removeImage($images,$magazine);

                }

            }
            $magazine->setBookWidth(300);
            $magazine->setBookHeight(463);
            $magazine->setUpdated(new \DateTime('now'));
            $this->flush($magazine);
            return $this->redirectToRoute('nrm_pariwar_magazine_list');
        }
        $data['form']= $form->createView();
        $data['pagination']= $this->get('em')->getRepo(Magazine::class)->findBy(['deleted'=>false]);

        $data['place']='Category';
        $data['title']='Add';
        $data['images']= $images;
        return $this->render('NRMPariwarMagazineBundle:Magazine:add.html.twig',$data);

    }

    public function removeImage($image,$magazine)
    {
        $filePath = $this->container->getParameter("kernel.root_dir") . "/../web/uploads/media/";

        $attachment = $this->get('em')->getRepo(Attachment::class)->findOneBy(['file'=>$image]);
        if($attachment!=null){
            if(file_exists($filePath.$attachment->getFile()))
            {
                unlink($filePath.$attachment->getFile());
            }
            if(file_exists($filePath.$attachment->getZoomUrl()))
            {
                unlink($filePath.$attachment->getZoomUrl());
            }
            $magazine->removeAttachment($attachment);
            $this->get('em')->remove($attachment);

        }


    }


    public  function deleteAction(Request $request)
    {
        $id = $request->get('id');
        if(!$id){
            return $this->redirectToRoute('nrm_pariwar_magazine_list');

        }

        $magazine = $this->get('em')->find(Magazine::class,$id);
        if($magazine->isDeleted()==true){
            return $this->redirectToRoute('nrm_pariwar_magazine_list');
        }
        $magazine->setDeleted(true);
        $this->get('em')->flush($magazine);
        return $this->redirectToRoute('nrm_pariwar_magazine_list');

    }


    public function flush($timeTest)
    {

        try {

            $this->get('em')->flush($timeTest);
            $this->addFlash('success', 'Successfully Done!!');
        } catch (\Exception $e) {

            $this->addFlash('error', 'Problem While Saving The Content!!');
        }
    }


}
