<?php

namespace NRMPariwar\MagazineBundle\Controller;

use NRMPariwar\MagazineBundle\Entity\Category;
use NRMPariwar\MagazineBundle\Form\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends Controller
{
    public function addAction(Request $request)
    {
        $id = $request->get('id');
        if($id){

            $category =  $this->get('em')->find(Category::class,$id);
            if($category->isDeleted()==true){
                $this->addFlash('error','Not Existed!!');
                return $this->redirectToRoute('nrm_pariwar_magazine_category_add');

            }
        }else{

            $category = new Category();
        }
        $form = $this->createForm(CategoryType::class,$category);
        $form->handleRequest($request);
        if($form->isSubmitted()&& $form->isValid()) {
            $category = $form->getData();
            $this->flush($category);
            return $this->redirectToRoute('nrm_pariwar_magazine_category_add');
        }
        $data['form']= $form->createView();
        $data['pagination']= $this->get('em')->getRepo(Category::class)->findBy(['deleted'=>false]);

        $data['place']='Category';
        $data['title']='List';
        return $this->render('NRMPariwarMagazineBundle:Category:list.html.twig',$data);

    }


    public  function deleteAction(Request $request)
    {
        $id = $request->get('id');
        if(!$id){
            return $this->redirectToRoute('nrm_pariwar_magazine_category_add');

        }

        $category = $this->get('em')->find(Category::class,$id);
        if($category->isDeleted()==true){
            return $this->redirectToRoute('nrm_pariwar_magazine_category_add');
        }
        $category->setDeleted(true);
        $this->get('em')->flush($category);
        return $this->redirectToRoute('nrm_pariwar_magazine_category_add');

    }

    public  function testingAction(Request $request)
    {
        $src = $this->get('kernel')->getRootDir()."/../web/uploads/media/2017_May_03/nagarik/1.jpg";
        $dst = $this->get('kernel')->getRootDir()."/../web/uploads/media/2017_May_03/nagarik/zoom_1.jpg";
        $resize= $this->get('resizer')->resize($src,$dst);
        dump('done');
        die();


//        http://localhost:7080/uploads/media/2017_May_03/nagarik/2.png
    }




    public function flush($timeTest)
    {

        try {

            $this->get('em')->flush($timeTest);
            $this->addFlash('success', 'Successfully Done!!');
        } catch (\Exception $e) {

            $this->addFlash('error', 'Problem While Saving The Content!!');
        }
    }


}
