<?php

namespace NRMPariwar\UserBundle\Service;

class EntityManagerService{

    private $em;

    public function __construct($doctrine)
    {
        $this->em = $doctrine->getManager();
    }

    public  function persist($object)
    {
        $this->em->persist($object);

    }

    public  function remove($object)
    {

        $this->em->remove($object);
    }

    public  function flushOnly()
    {

        $this->em->flush();
    }

    public  function flush($object)
    {

        $this->em->persist($object);
        $this->em->flush();
    }

    public  function getRepo($object){

        return $this->em->getRepository($object);
    }

    public  function find($object,$id){

        return $this->getRepo($object)->find($id);
    }




}