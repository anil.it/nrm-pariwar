<?php

namespace NRMPariwar\UserBundle\Service;



class ImageResizerService{

    public  function createNewShape($sTempFileName)
    {

        $aSize = @getimagesize($sTempFileName);
        $w = 0;
        $h = 0;
        $r = 0;

        if ($aSize[1] <= $aSize[0] and $aSize[0] != 0) {

            $r = $aSize[1] * (1.0) / $aSize[0];
            $w = 922;
            $h = $r * $w;
        } elseif ($aSize[1] > $aSize[0] and $aSize[1] != 0) {
            $r = $aSize[0] * (1.0) / $aSize[1];
            $h = 600;
            $w = $r * $h;

        }
        $image['tar_w']=$w;
        $image['tar_h']=$h;

        $image['dst'] = @imagecreatetruecolor($image['tar_w'], $image['tar_h']);
        if (!$aSize) {
            @unlink($sTempFileName);

        }
        return $image;


    }

    public  function compress($sTempFileName,$destinationFile,$image)
    {
        $aSize = @getimagesize($sTempFileName);
        $img_r = null;
        switch ($aSize[2]) {
            case IMAGETYPE_JPEG:
                $img_r = @imagecreatefromjpeg($sTempFileName);
                @imagecopyresampled($image['dst'], $img_r, 0, 0, 0, 0,
                    $image['tar_w'], $image['tar_h'], $aSize[0], $aSize[1]);
                copy($sTempFileName, $destinationFile);
                imagejpeg($image['dst'], $destinationFile, 50);
                break;

            case IMAGETYPE_PNG:
                @imageAlphaBlending($img_r, true);
                @imageSaveAlpha($img_r, true);
                $img_r = @imagecreatefrompng($sTempFileName);
                imagecopyresampled($image['dst'], $img_r, 0, 0, 0, 0,
                    $image['tar_w'], $image['tar_h'], $aSize[0], $aSize[1]);
                copy($sTempFileName, $destinationFile);
                imagepng($image['dst'], $destinationFile, 5);
                break;

        }



    }

    public  function compressN($sTempFileName,$image)
    {
        $aSize = @getimagesize($sTempFileName);
        $img_r = null;
        switch ($aSize[2]) {
            case IMAGETYPE_JPEG:
                $img_r = @imagecreatefromjpeg($sTempFileName);
                @imagecopyresampled($image['dst'], $img_r, 0, 0, 0, 0,
                    $image['tar_w'], $image['tar_h'], $aSize[0], $aSize[1]);
                imagejpeg($image['dst'], $sTempFileName, 50);
                break;

            case IMAGETYPE_PNG:
                @imageAlphaBlending($img_r, true);
                @imageSaveAlpha($img_r, true);
                $img_r = @imagecreatefrompng($sTempFileName);
                imagecopyresampled($image['dst'], $img_r, 0, 0, 0, 0,
                    $image['tar_w'], $image['tar_h'], $aSize[0], $aSize[1]);
                imagepng($image['dst'], $sTempFileName, 5);
                break;

        }



    }

    public function resize($sTempFileName,$destinationFile)
    {
        $images = $this->createNewShape($sTempFileName);
        $this->compress($sTempFileName,$destinationFile,$images);
    }



    public function resizePreview($sTempFileName)
    {
        $images = $this->createNewShape($sTempFileName);
        $this->compressN($sTempFileName,$images);

    }









}