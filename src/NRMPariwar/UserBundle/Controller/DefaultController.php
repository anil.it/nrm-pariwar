<?php

namespace NRMPariwar\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('NRMPariwarUserBundle:Default:index.html.twig');
    }
}
