<?php

namespace NRMPariwar\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $data['title']='Dashboard';
        $data['place']='Home';
        return $this->render('NRMPariwarAdminBundle:Default:index.html.twig',$data);
    }
}
