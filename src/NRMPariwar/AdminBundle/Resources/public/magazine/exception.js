/**
 * @author Anil Silwal
 */
;
(function () {
    magazine.core.Exception = function (message) {
        this.message = message;
        this.toString = function () {
            return this.message;
        };
    }
})();
