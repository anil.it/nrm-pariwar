$(document).ready(function() {

    // Responsive Menu
        $('.btn_nav').click(function(){
            $(this).find('i').toggleClass('fa-bars fa-close')
            $('body').toggleClass('scroll-hide');
            $('nav').toggleClass('show');
        });

    // Datepicker
    $('#from, #to').datetimepicker({
        format: 'd-m-Y',
        formatDate: 'd-m-Y',
        timepicker: false,
        maxDate: 0,
    });

    // Timepicker
    $('#time').datetimepicker({
        format: 'H:m:s',
        formatDate: 'H:m:s',
        datepicker: false,
    });


}); // END OF - DOCUMENT READY FUNCTION

       

