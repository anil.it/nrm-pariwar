<?php

namespace NRMPariwar\FrontendBundle\Controller;

use NRMPariwar\MagazineBundle\Entity\Attachment;
use NRMPariwar\MagazineBundle\Entity\Magazine;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $id = $request->get('id');
        if($id != null)
        {
            $data['magazine']=$this->get('em')->getRepo(Attachment::class)->getLatestOne($id);

        }else{
            $magazine=$this->get('em')->getRepo(Magazine::class)->getLatestOne();
            $data['magazine']=$this->get('em')->getRepo(Attachment::class)->getLatestOne($magazine[0]['m_id']);

        }

        $data['title']='Magazine';
        return $this->render('NRMPariwarFrontendBundle:Default:index.html.twig',$data);
    }
    public function archiveAction(Request $request)
    {

        $qb=$this->get('em')->getRepo(Magazine::class)->getAll();
        $paginator  = $this->get('knp_paginator');
        $data['magazine'] = $paginator->paginate($qb,$request->query->getInt('page', 1), 3);


        $data['title']='Archive';
        return $this->render('NRMPariwarFrontendBundle:Archive:index.html.twig',$data);
    }



}
